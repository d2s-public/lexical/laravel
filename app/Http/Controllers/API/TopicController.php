<?php

namespace App\Http\Controllers\API;

use App\Models\Topic;
use App\Http\Requests\StoreTopicRequest;
use App\Http\Requests\UpdateTopicRequest;

class TopicController extends APIController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        return response()->json(Topic::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreTopicRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTopicRequest $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Topic $topic
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateTopicRequest $request
     * @param \App\Models\Topic                     $topic
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicRequest $request, Topic $topic) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Topic $topic
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic) {
        //
    }
}
