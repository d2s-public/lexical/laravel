<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/**
 * Public routes
 */
Route::get('entry/all', [EntryController::class, 'index']);
Route::get('entry/{id}', [EntryController::class, 'show']);
Route::get('entry/find/{query}', [EntryController::class, 'find']);

/**
 * Private: that need to authenticate user
 */
Route::apiResource('entry', EntryController::class);
Route::group(['middleware'=>['auth:sanctum']], function () {
    Route::post('entry', [EntryController::class, 'store']);
    Route::put('entry/{entry', [EntryController::class, 'store']);
    Route::delete('entry/{entry}', [EntryController::class, 'store']);
});
