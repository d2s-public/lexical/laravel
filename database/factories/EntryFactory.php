<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Entry;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Entry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'headword'=>$this->faker->word,
            'part_of_speech'=>$this->faker->randomElement([]),
            'meaning'=>$this->faker->sentences,
            'example'=>$this->faker->sentence,
            'variant'=>$this->faker->words,
            'synonyms'=>$this->faker->randomNumber(1),
            'author'=>$this->faker->randomNumber(1)
        ];
    }
}
